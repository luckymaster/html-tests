<?php
if(!empty($_GET)) {
    $result = '';
    foreach ($_GET as $key => $val) {
        $result .= $key . ' => ' . $val . '<br>';
    }
}

?><!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <style>
            input {
                display: box;
            }
        </style>
    </head>
    <body>
        <?php if($result): ?>
            <h1><?php echo $result;?></h1>
        <?php endif;?>
        <form>
            <input type='text' name='text1' placeholder="введите текст1"/>
            <input type='text' name='text2' placeholder="введите текст2"/>
            <input type='text' name='text3' placeholder="введите текст3"/>
            <input type="text" id="uname" name="uname" required
               minlength="4" maxlength="8"
               placeholder="4 to 8 characters long" />
            <input type="submit" value="Отправить форму">
        </form>
    </body>
</html>
<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

